<?php

namespace App\Http\Controllers;

use App\Http\Requests\ApiTaskRequest;
use App\Services\ReorderTasksOnCategoryAndTaskOrderChange;
use App\Task;
use Illuminate\Support\Facades\Auth;


class TaskController extends Controller
{
    public function index()
    {
        return response()->json(Task::all()->toArray());
    }

    public function store(ApiTaskRequest $request)
    {
        $data = $request->only(['name', 'category_id', 'order']);
        $data["user_id"] = Auth::user()->id;
        $task = Task::create($data);

        return response()->json([
            'status' => (bool)$task,
            'data' => $task,
            'message' => $task ? __('New Task Successfully Created!') : __('Error Creating New Task')
        ]);
    }

    public function show(ApiTaskRequest $request, Task $task)
    {
        return response()->json($task);
    }

    public function update(ApiTaskRequest $request, Task $task)
    {

        $reorderService = new ReorderTasksOnCategoryAndTaskOrderChange();
        $category_id = $task->category_id;
        $data = $request->only(['name', 'category_id', 'order']);
        $status = $task->update($data);
        if (array_key_exists('category_id', $data)) {
            $reorderService->reorder($data['category_id']);
            if ($data["category_id"] != $category_id) {
                $reorderService->reorder($category_id);
            }
        }

        return response()->json([
            'status' => $status,
            'message' => $status ? __('Task Updated!') : __('Error Updating Task')
        ]);
    }

    public function destroy(ApiTaskRequest $request, Task $task)
    {
        $status = $task->delete();

        return response()->json([
            'status' => $status,
            'message' => $status ? __('Task Deleted!') : __('Error Deleting Task')
        ]);
    }

}
