window.axios = require('axios');

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

/**
 * Next we will register the CSRF Token as a common header with Axios so that
 * all outgoing HTTP requests automatically have it attached. This is just
 * a simple convenience so we don't have to attach every token manually.
 */

let token = document.head.querySelector('meta[name="csrf-token"]');

if (token) {
    window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
} else {
    console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
}
import Vue from 'vue'
import VueRouter from 'vue-router'
import Toasted from 'vue-toasted';

Vue.use(Toasted)
Vue.use(VueRouter)
window.axios.interceptors.response.use(response => {
        if (response.data['message'] !== undefined && response.status === 200) {
            Vue.toasted.success(response.data.message, {
                position: 'bottom-right',
                fitToScreen: true,
                closeOnSwipe: true,
                duration: 3000
            });
        }


        return response;
    },
    error => {
        Vue.toasted.error(error.response.data.message, {
            position: 'bottom-right',
            fitToScreen: true,
            closeOnSwipe: true,
            duration: 3000
        });

        return error;

    }
);

import App from './components/App'
import Home from './components/Home'
import Dashboard from './components/Dashboard'
import Login from './components/Login'
import Logout from './components/Logout'
import Register from './components/Register'

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home
        },
        {
            path: '/login',
            name: 'login',
            component: Login,
        },
        {
            path: '/logout',
            name: 'logout',
            component: Logout,
        },
        {
            path: '/register',
            name: 'register',
            component: Register,
        },
        {
            path: '/board',
            name: 'board',
            component: Dashboard,
        },
    ],
});

const app = new Vue({
    el: '#app',
    components: {App},
    router,
});