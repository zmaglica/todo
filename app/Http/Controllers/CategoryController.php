<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Requests\ApiCategoryRequest;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(Category::all()->toArray());
    }

    public function tasks(Category $category)
    {
        return response()->json($category->tasks()->orderBy('order')->get());
    }

    public function store(ApiCategoryRequest $request)
    {
        $category = Category::create($request->only('name'));
        return response()->json([
            'status' => (bool) $category,
            'message'=> $category ? __('Category successfully created.') : __('Error creating category.'),
            'data' => $category,
        ]);
    }

    public function show(Category $category)
    {
        return response()->json($category);
    }

    public function update(ApiCategoryRequest $request, Category $category)
    {
        $status = $category->update($request->only('name'));

        return response()->json([
            'status' => $status,
            'message' => $status ? __('Category Updated!') : __('Error Updating Category')
        ]);
    }

    public function destroy(Category $category)
    {
        $status  = $category->delete();

        return response()->json([
            'status' => (bool)$status,
            'message' => $status ? __('Category Deleted') : __('Error Deleting Category')
        ]);
    }

}
