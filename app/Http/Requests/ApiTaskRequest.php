<?php

namespace App\Http\Requests;


use Illuminate\Support\Facades\Auth;

class ApiTaskRequest extends ApiFormRequests
{
    /**
 * Determine if the user is authorized to make this request.
 *
 * @return bool
 */
    public function authorize()
    {
        return $this->task->user_id == $this->user()->id;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->route()->getName() === "api.task"){
            return [
                'name' => 'required',
                'order' => 'required|numeric|min:0',
                'category_id' => 'required|exists:categories,id',
            ];
        }
        return [
            'name' => 'nullable',
            'order' => 'nullable|numeric|min:0',
            'category_id' => 'nullable|exists:categories,id',
        ];

    }
}
