<?php

namespace Tests\Unit;

use App\Category;
use App\Task;
use App\User;
use Tests\TestCase;

class ApiTaskTest extends TestCase
{
    private $user;
    private $category;

    public function setUp()
    {
        parent::setUp();
        $this->user = User::first();
        $this->category = Category::first();
    }


    public function testTaskCreate()
    {

        $response = $this->actingAs($this->user, 'api')->json('POST', '/api/task', [
            'name' => str_random(10),
            'category_id' => $this->category->id,
            'order' => 99,
            'user_id' => $this->user->id,
        ]);

        $response->assertStatus(200)->assertJsonStructure([
            'status',
            'message',
            'data'
        ]);
    }

    public function testTaskDeletion()
    {
        $task = Task::orderBy('id', 'desc')->first();
        $response = $this->actingAs($this->user, 'api')
            ->json('DELETE', "/api/task/{$task->id}");
        $response->assertStatus(200)->assertJson([
            'status' => true,
            'message' => __('Task Deleted!')
        ]);
    }
}
