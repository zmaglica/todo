<?php

namespace Tests\Unit;

use App\User;
use Tests\TestCase;

class ApiCategoryTest extends TestCase
{
    private $user;

    public function setUp()
    {
        parent::setUp();
        $this->user = User::first();
    }

    public function testCategoryList()
    {
        $response = $this->actingAs($this->user, 'api')
            ->json('GET', '/api/category');

        $response->assertStatus(200)->assertJsonStructure([
                '*' => [
                    'id',
                    'name',
                    'created_at',
                    'updated_at',
                    'deleted_at'
                ]
            ]
        );
    }

    public function testCategoryCreate()
    {

        $response = $this->actingAs($this->user, 'api')->json('POST', '/api/category', [
            'name' => str_random(10),
        ]);

        $response->assertStatus(200)->assertJsonStructure([
            'status',
            'message',
            'data'
        ]);
    }

    public function testCategoryDeletion()
    {
        $category = \App\Category::orderBy('id', 'desc')->first();
        $response = $this->actingAs($this->user, 'api')
            ->json('DELETE', "/api/category/{$category->id}");
        $response->assertStatus(200)->assertJson([
            'status' => true,
            'message' => __('Category Deleted')
        ]);
    }
}
