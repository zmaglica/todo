<?php

namespace App\Services;


use App\Task;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ReorderTasksOnCategoryAndTaskOrderChange
{


    public function reorder($category)
    {
        DB::transaction(function () use ($category) {
            DB::statement("SET @running_order=-1");
            DB::update("UPDATE tasks SET tasks.order = @running_order:=@running_order + 1  WHERE deleted_at IS NULL AND category_id = ? AND user_id = ? ORDER BY tasks.order ASC, tasks.updated_at DESC;", [$category, Auth::user()->id]);
        });
    }


}