<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        User::create([
            'name' => 'Test user',
            'email' => 'user@example.com',
            'password' => bcrypt('user'),
        ]);
    }
}
