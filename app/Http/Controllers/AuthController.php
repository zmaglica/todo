<?php

namespace App\Http\Controllers;


use App\Http\Requests\ApiAuthRequest;
use App\Http\Requests\ApiRegisterRequest;
use Illuminate\Support\Facades\Auth;
use App\User;


class AuthController extends Controller
{
    public function login(ApiAuthRequest $request)
    {
        if (Auth::attempt($request->only('email', 'password'))) {
            $success['token'] = Auth::user()->createToken('MyApp')->accessToken;

            return response()->json(['success' => $success]);
        }

        return response()->json([
            'message' => __('Invalid credentials')
        ], 401);
    }

    public function register(ApiRegisterRequest $request)
    {

        $input = $request->except('_token');;
        $input['password'] = bcrypt($input['password']);

        $user = User::create($input);
        $success['token'] = $user->createToken('MyApp')->accessToken;
        $success['name'] = $user->name;

        return response()->json(['success' => $success]);
    }

    public function logout()
    {
        if (Auth::check()) {
            Auth::user()->token()->revoke();
        }
        return response()->json(['success' => 'You have successfully logged out']);
    }

    public function user()
    {
        return response()->json(['success' => Auth::user()]);
    }

}